Fuel Economy Calculator
=======================

It's a simple page to calculate which is the best fuel to fill in your car, based on the gas and ethanol consumptions.

Developed using HTML/CSS/Bootstrap/JavaScript

