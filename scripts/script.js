
function calcFuelEconomy()
{ 
  var kml_gas = document.getElementById("kml_gas").value
                                                  .replace(',','.');
  var kml_ethanol = document.getElementById("kml_ethanol").value
                                                          .replace(',','.');
  var kml_rate;
  
  var price_gas = document.getElementById("price_gas").value
                                                      .replace(',','.');
  var price_ethanol = document.getElementById("price_ethanol").value
                                                              .replace(',','.');
  var price_rate;
      
  var km_to_go_gas;
  var km_to_go_ethanol;
  var fuel_choice;

  kml_gas = isNaN(parseFloat(kml_gas)) ? 0 : parseFloat(kml_gas);
  kml_ethanol = isNaN(parseFloat(kml_ethanol)) ? 0 : parseFloat(kml_ethanol);
  kml_rate = kml_ethanol / kml_gas
  price_gas = isNaN(parseFloat(price_gas)) ? 0 : parseFloat(price_gas);
  price_ethanol = isNaN(parseFloat(price_ethanol)) ? 
                                                  0 : parseFloat(price_ethanol);
  price_rate = price_ethanol / price_gas; 
  
  km_to_go_gas = ((100/price_gas) * kml_gas).toFixed(2);
  km_to_go_ethanol = ((100/price_ethanol) * kml_ethanol).toFixed(2);
  
  /**********************************/
  /* Check which fuel it's the best */
  /**********************************/
  if ((kml_rate > 0  && price_rate > 0) && (kml_rate == price_rate))
  {
    fuel_choice = "Tanto Faz"
  } else {
    if (kml_rate > price_rate)
    {
      fuel_choice = " Álcool "
    } else {
      fuel_choice = " Gasolina "    
    }
  }
  
  /************************************/
  /* Print the summary of consumption */
  /************************************/  
  document.getElementById('calculation_summary').style.visibility="visible";
  
  if ( kml_gas > 0 && kml_ethanol >= 0 && 
       price_gas > 0 && price_ethanol >= 0)
  {
    document.getElementById('fuel_choice').innerHTML = fuel_choice;
    document.getElementById('gas_calc').innerHTML = km_to_go_gas;
    if (kml_ethanol > 0 && price_ethanol > 0)
    {
      document.getElementById('ethanol_calc').style.visibility="visible";
      document.getElementById('ethanol_calc').innerHTML = km_to_go_ethanol;      
    } else {
      document.getElementById('ethanol_calc').style.visibility="hidden";
    }                 
    
    summaryFadeIn();
    
  } else {
    alert("Valores Inválidos! Favor verificá-los!");
  }
      
}
    
function resetCalc()
{
  inputs = document.getElementsByTagName('input')
  for(i=0;i<inputs.length;i++)
  {
    if ( inputs[i].type == "text" )
    {
       inputs[i].value = ""
    }
  }
  
  summaryFadeOut();
  
}

function initProcess()
{  
  document.getElementById('calculation_summary').style.visibility="hidden";
  document.getElementById('ethanol_calc').style.visibility="hidden";
 
  resetCalc(); 
}